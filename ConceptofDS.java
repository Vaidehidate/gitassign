package datastructure;
import java.util.*;
import static java.util.stream.IntStream.*;

public class ConceptofDS {
	
	
	public static void main(String[] args) 
    {
        System.out.println();
        System.out.println("linked list starts");
        linkedList();

         System.out.println();
         Random rand = new Random();
        Stack<Integer> stk = new Stack<>();
        
        System.out.println("Stack starts");
        System.out.println("Stack implementation ");
        System.out.println("stack: " + stk);
        // pushing elements into the stack
        pushelmnt(stk, 20);
        pushelmnt(stk, 65);
        pushelmnt(stk, 11);
        pushelmnt(stk, 45);
        pushelmnt(stk, 90);
        pushelmnt(stk, 95);
        pushelmnt(stk, 15);
        pushelmnt(stk, 18);

        // popping elements from the stack
        popelmnt(stk);
        popelmnt(stk);
        stack_peek(stk);
        System.out.println("The stack size is: " + stk.size());
        for (int i=0; i>5; i++){
            stk.push(rand.nextInt());
        }

         System.out.println("Stack: " +stk);
         //System.out.println(stk);

         System.out.println("Reversed Stack:");
         System.out.println(reverse(stk));
        
         System.out.println();
         System.out.println("**********************************");
      
        queue();
        priortyqueue();
        hashTable();
    }

    static void linkedList()
    {
      LinkedList<String> list1 = new LinkedList<String>();
      System.out.println("Linkedlist implementation");
     // Adding ele.
        list1.add("2");
        list1.add("4");
        list1.add("5");
        list1.add("6");
        list1.addLast("7");
        list1.addFirst("1");
        list1.add(2, "8");
        System.out.println("Link list: " +list1);
        list1.iterator();
        list1.removeLast();
        list1.remove("6");
        System.out.println("Updated list : " +list1);
        
        String middle = list1.get(list1.size()/2);
        System.out.println("Center : " +middle);

       // list1.sort(Comparator.reverseOrder());
       Collections.sort(list1);
        System.out.println("Sort : " +list1);
        Collections.reverse(list1);
        System.out.println("Reverse List : " +list1);
        System.out.println("The list size is: " +list1.size());

         Iterator<String> it = list1.iterator();
      while(it.hasNext())
      {
          System.out.println("The item of list : " +list1.poll());
      }   
      
      System.out.println();
          System.out.println("**************************************");
    }

    static void pushelmnt(Stack stk, int x) {

        stk.push(new Integer(x));
        System.out.println("push -> " + x);
        // prints modified stack
        System.out.println("stack: " + stk);
    }

    // performing pop operation
    static void popelmnt(Stack stk) {
        System.out.print("pop -> ");
        // invoking pop() method
        Integer x = (Integer) stk.pop();
        System.out.println(x);
        // prints modified stack
        System.out.println("stack: " + stk);
    }

    static void stack_peek(Stack<Integer> stk) {
        Integer element = (Integer) stk.peek();
        System.out.println("Element on stack peek: " + element);
    }
    
    public static Queue reverse(Stack arr)
    {
    Queue<Integer> arrCopy=new LinkedList<Integer>();
    while(!arr.empty())
    {
        arrCopy.add((int)arr.pop());
    }
   return arrCopy;
    
}
    

   //System.out.println();

    public static void queue() 
    {

        System.out.println();
        System.out.println("Queue starts");
        System.out.println("Queue implementation");

        Queue<Integer> q = new LinkedList<>();
        ArrayList<Integer> arr = new ArrayList<>();
        // Add ele. 0, 1, 2, 3, 4
        for (int i = 0; i < 5; i++)
            q.add(i);

        // Display
        System.out.println("Enqueue " + q);

        // Remove
        int removedele = q.remove();
        System.out.println("Dequeue " + removedele);

        System.out.println("New queue " + q);

        // To view the head of queue
        int head = q.peek();
        System.out.println("Element on queue peek : " + head);

        // Rest all methods
        int size = q.size();
        System.out.println("Queue size " + size);
       // pushelmnt.iterator();
       Collections.sort(arr);
    for(int i=0;i<arr.size();i++)
    {
        q.add(arr.get(i));
    }
    System.out.println("Sort : " +q);
    System.out.println();
    System.out.println("**************************************");
        
    }
    private static void priortyqueue() {
      System.out.println();
      System.out.println("priorty queue starts");
      PriorityQueue<String> pQueue = new PriorityQueue<>();
      
      // Add
      pQueue.add("10");
      pQueue.add("20");
      pQueue.add("15");
      pQueue.add("25");
      pQueue.add("35");
      System.out.println(pQueue);
    
    // System.out.println("Peek : " +pQueue.peek());
      System.out.println(pQueue.poll());
      System.out.println(pQueue);
     System.out.println("Peek : " +pQueue.peek());
     System.out.println("Contains ");
            System.out.println("Is the value 15 present? " + pQueue.contains(15));
  
            System.out.println("Is the value 9 present? " + pQueue.contains(9));
            System.out.println();
    System.out.println("Size of priority queue : " +pQueue.size());
      Iterator<String> it = pQueue.iterator();
      while(it.hasNext())
      {
          System.out.println("The item of queue : " +pQueue.poll());
      }    
      System.out.println();
      System.out.println("********************************************");
    }
     private static void hashTable() 
    {

        System.out.println();
        System.out.println("Hash Table starts");

                Hashtable<Integer, String> hashtable1 = new Hashtable<>();

                hashtable1.put(1, "one");
                hashtable1.put(2, "two");
                hashtable1.put(3, "three");

                hashtable1.put(4, "four");
                hashtable1.put(5, "five");
                hashtable1.put(6, "six");

            System.out.println("Mappings of initial ht1 : " + hashtable1);
            System.out.println("Insert ");
                 String returned_value = (String)hashtable1.put(7, "seven");
            System.out.println("Mappings of new ht1 : " + hashtable1);
                String returned_value1 = (String)hashtable1.remove(4);
            System.out.println("Delete ");
               // System.out.println("Mappings of updated ht1 : " + returned_value1);
            System.out.println("Mappings of updated ht1 is: " + hashtable1);
            System.out.println("Contains ");
            System.out.println("Is the value 'fiva' present? " + hashtable1.containsValue("five"));
  
            System.out.println("Is the value 'nine' present? " + hashtable1.containsValue("nine"));
            System.out.println();
            System.out.println("Get Value by Key ");
                 Enumeration enu = hashtable1.keys();

                while (enu.hasMoreElements()) 
                {
 
                      // Displaying the Enumeration
                         System.out.println(enu.nextElement());
                }

            System.out.println("Size of hashtable : " +hashtable1.size());
            
            System.out.println("Iterator ");
              Set<Integer> setOfKeys = hashtable1.keySet();
            Iterator<Integer> itr = setOfKeys.iterator();
 
        // Iterating through the Hashtable object
        // Checking for next element using hasNext() method
         while(itr.hasNext())
         {
 
            // Getting key of a particular entry
            int key = itr.next();
 
            // Print and display the Rank and Name
            System.out.println("Rank : " + key + "\t\t Name : " + hashtable1.get(key));
        }
           System.out.println("Hashtable is : " + hashtable1);
    }

}
