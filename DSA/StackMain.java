package com.nagarro.stack;

//package Sample.stack;

public class StackMain {
  public static void main(String[] args) {
      Stack<Integer> stack = new Stack<Integer>(5);
      

      stack.push(1);
      stack.push(22);
      stack.push(3);
      stack.push(4);


      stack.pop();
      System.out.println("\nAfter popping out");
      stack.push(78);

      stack.printStack();
      System.out.println("The peek element is " + stack.peek());
      System.out.println("The stack size is " + stack.size());
      
      
  }
}


