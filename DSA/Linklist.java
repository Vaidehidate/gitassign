package com.nagarro.datastructure;
import java.util.*;
	public class Linklist 
	{

			 Node head; 
			static class Node 
			{

				int data;
				Node next;

				// Constructor
				Node(int d)
				{
					data = d;
					next = null;
				}
				public void displayNodeData() 
				 {
					  System.out.println(" { " + data + " }  " );
					 
					 }
				 
			}

			// Method to insert a new node
			public static Linklist insert(Linklist list, int data)
			{
				// Create a new node with given data
				Node node = new Node(data);
				node.next = null;

				if (list.head == null) {
					list.head = node;
				}
				else 
				{
					
					Node last = list.head;
					while (last.next != null) {
						last = last.next;
					}
					
					last.next = node;
				}

				return list;
			}

			// Method to print the LinkedList.
			public static void printList(Linklist list)
			{
				Node currNode = list.head;
			
				System.out.print("LinkedList: ");
			
				// Traverse through the LinkedList
				while (currNode != null) {
					// Print the data at current node
					System.out.print(currNode.data + " ");
			
					// Go to next node
					currNode = currNode.next;
				}
			}
			
			public void insertNth(int data, int position) 
			{
			        Node node = new Node(data);
			        node.data = data;
			        node.next = null;


			        if (this.head == null) 
			{
			            //if head is null and position is zero then exit.
			            if (position != 0) 
			{
			                return;
			            } 
			else 
			{ 
			//node set to the head.
			                this.head = node;
			   }
			        }


			if (head != null && position == 0) 
			{
			            node.next = this.head;
			            this.head = node;
			            return;
			   }

			        Node current = this.head;
			        Node previous = null;

			        int i = 0;

			        while (i < position) 
			{
			            previous = current;
			            current = current.next;

			            if (current == null)
			 {
			                break;
			            }

			            i++;
			        }

			        node.next = current;
			        previous.next = node;
			    }
			void deleteNode(int position)
		    {
		        // If linked list is empty
		        if (head == null)
		            return;
		 
		        // Store head node
		        Node temp = head;
		 
		        // If head needs to be removed
		        if (position == 0) {
		            head = temp.next; // Change head
		            return;
		        }
		 
		        // Find previous node of the node to be deleted
		        for (int i = 0; temp != null && i < position - 1;
		             i++)
		            temp = temp.next;
		 
		        // If position is more than number of nodes
		        if (temp == null || temp.next == null)
		            return;
		 
		        
		        Node next = temp.next.next;
		 
		        temp.next = next; // Unlink the deleted node from list
		    }
			void printMiddle()
		    {
		        Node slow_ptr = head;
		        Node fast_ptr = head;
		         
		            while (fast_ptr != null && fast_ptr.next != null)
		            {
		                fast_ptr = fast_ptr.next.next;
		                slow_ptr = slow_ptr.next;
		            }
		            System.out.println("The middle element is [" +
		                                slow_ptr.data + "] ");
		         
		    }
			 public void sortList()
			    {
			 
			        // Node current will point to head
			        Node current = head, index = null;
			 
			        int temp;
			 
			        if (head == null) {
			            return;
			        }
			        else {
			            while (current != null) {
			                // Node index will point to node next to
			                // current
			                index = current.next;
			 
			                while (index != null) {
	// If current node's data is greater than index's node data, swap the data between them
			                    if (current.data > index.data) {
			                        temp = current.data;
			                        current.data = index.data;
			                        index.data = temp;
			                    }
			 
			                    index = index.next;
			                }
			                current = current.next;
			            }
			        }
			    }
			 public int sizeofList()
			 {
			    Node temp=head;
			    int count = 0;
			    while(temp!=null)
			    {
			   temp=temp.next;
			   count++;  
			    }
			     return count;
			 }
			 
				
			 public void printLinkedList() {
				
				  Node current = head;
				  while (current != null) {
				   current.displayNodeData();
				   current = current.next;
				 
				  }

			 }
			 
	}
	
	


