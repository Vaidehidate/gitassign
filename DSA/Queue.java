package com.nagarro.queue;
import java.util.LinkedList;
	public class Queue 
	{

		 private int[] arr;      // array to store queue elements
		    private int front;      // front points to the front element in the queue
		    private int rear;       // rear points to the last element in the queue
		    private int capacity;   // maximum capacity of the queue
		    private int count;      // current size of the queue
		 
		    // Constructor to initialize a queue
		    Queue(int size)
		    {
		        arr = new int[size];
		        capacity = size;
		        front = 0;
		        rear = -1;
		        count = 0;
		    }
		    public int dequeue()
		    {
		        // check for queue underflow
		        if (isEmpty())
		        {
		            System.out.println("Underflow\nProgram Terminated");
		            System.exit(-1);
		        }
		 
		        int x = arr[front];
		 
		        System.out.println("Dequeue " + x);
		 
		        front = (front + 1) % capacity;
		        count--;
		 
		        return x;
		    }
		    public void enqueue(int item)
		    {
		        // check for queue overflow
		        if (isFull())
		        {
		            System.out.println("Overflow\nProgram Terminated");
		            System.exit(-1);
		        }
		 
		        System.out.println("Enqueue " + item);
		 
		        rear = (rear + 1) % capacity;
		        arr[rear] = item;
		        count++;
		    }
		    public int peek()
		    {
		        if (isEmpty())
		        {
		            System.out.println("Underflow\nProgram Terminated");
		            System.exit(-1);
		        }
		        return arr[front];
		    }
		    
		    // Utility function to return the size of the queue
		    public int size() {
		        return count;
		    }
		    
		    public boolean isEmpty() {
		        return (size() == 0);
		    }
		 
		    // Utility function to check if the queue is full or not
		    public boolean isFull() {
		        return (size() == capacity);
		    }
		    static void sortQueue(java.util.Queue<Integer> queue) {
				int n = queue.size();
				for (int i = 0; i < n; i++) {
					// Find the index of smallest element from the unsorted queue
					int minIndex = -1;
					int minValue = Integer.MAX_VALUE;
					for (int j = 0; j < n; j++) {
						int currValue = queue.poll();
						// Find the minimum value index only from unsorted queue
						if (currValue < minValue && j < (n - i)) {
							minValue = currValue;
							minIndex = j;
						}
						queue.add(currValue);
					}
					// Remove min value from queue
					for (int j = 0; j < n; j++) {
						int currValue = queue.poll();
						if (j != minIndex) {
							queue.add(currValue);
						}
					}
					// Add min value to the end of the queue
					queue.add(minValue);
				}
				// Print the sorted queue
				for (Integer i : queue) {
					System.out.print(i + " ");
				}
				System.out.println();
			}





	}


