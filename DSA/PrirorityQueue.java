package com.nagarro.priorityqueue;

public class PrirorityQueue {

		static Node head=null;
		 public static  class Node {
		    int data;
		    int priority;
		    Node next;
		 }
		 
		 static Node New(int d,int p){
			 Node temp=new Node();
			 temp.data = d;
	         temp.priority=p;
	         temp.next = null;
	         return temp;
		 }  
	      
		    public  void Enqueue(int data,int priority)
		    {
		       Node temp=New(data,priority);
		       Node start=head;
		       if(head==null) {
		   		temp.next=head;
		   		head=temp;
		   		return;
		   	}
		       if(head.priority>temp.priority) {
		    	   temp.next=head;
		    	   head=temp;
		       }
		       else {
		    	   while (start.next != null &&
		    				start.next.priority < temp.priority)
		    	   {
		    				start = start.next;
		    			}
		    		
		    			
		    			temp.next = start.next;
		    			start.next = temp;
		       }
		       return;
		        
		    }
		 
		   
		    public  void Dequeue()
		    {
		       
		      if(head.next==null) {
		    	  return;
		      }
		      Node point=head;
		      head=head.next;
		      return ;
		      
		       
		    }
		 
		    public  void peek()
		    {
		    	System.out.println(head.data);
		    }
		    
		    
		    public  void size() {
		    	int size=0;
		    	Node temp=head;
		    			while(temp!=null) {
		    				size++;
		    				temp=temp.next;
		    			}
		    	System.out.println(size);
		    }
		    

		    public     boolean contains(int x) {
		    	Node temp=head;
		    	while(temp!=null) {
		    		if(temp.data==x) {
		    			return true;
		    		}
		    		temp=temp.next;
		    	}
		    	return false;
		    	
		    }
		    
		    
		    public  void Center() {
		    	Node p1=head;
		    	Node p2=head;
		    	while(p2.next!=null) {
		    		p2=p2.next;
		    		if(p2.next!=null) {
		    			p2=p2.next;
		    			p1=p1.next;
		    		}
		    	}
		    	System.out.println(p1.data);
		    	
		    }

		    
		    public  void sort() {
		    	 Node current = head;
		 	    Node index = null;
		 	    int temp;

		 	    if (head == null) {
		 	      return ;
		 	    } else {
		 	      while (current != null) {
		 	        // index points to the node next to current
		 	        index = current.next;

		 	        while (index != null) {
		 	          if ((current.data) > (index.data)) {
		 	            temp = current.data;
		 	            current.data = index.data;
		 	            index.data = temp;
		 	          }
		 	          index = index.next;
		 	        }
		 	        current = current.next;
		 	      }
		 	    }
		 	    return;
		    	
		    }
		    
		    
		    public void reverse() {

		    	Node previous = null;  
		    	Node curr = head;  
		    	Node forward = null;  
		    	  
		    	  
		    	while (curr != null)   
		    	{  
		    	forward = curr.next;  
		    	curr.next = previous;  
		    	previous = curr;  
		    	curr = forward;  
		    	}  
		    	head=previous;
		    	
		    	 
		    	 return; 
		    	
		    }
		    
		    
		   
		    public  void Display()
		    {
		    	Node temp =head;
		    	while(temp!=null) {
		    		System.out.printf(temp.data+" <---");
		    		 
		    		temp=temp.next;
		    	}
		    }
		    
		 
		 
	
}

