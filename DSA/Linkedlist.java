
package com.nagarro.datastructure;

public class Linkedlist {
	public static void main(String[] args)
	{
		Linklist list = new Linklist();
				System.out.println( "------Insert the values------");
		list.insert(list, 2);
		list.insert(list, 3);
		list.insert(list, 4);
		list.insert(list, 5);
		list.insert(list, 9);
		list.insert(list, 6);
		list.insert(list, 8);
		// Print the LinkedList
		list.printList(list);
		System.out.println();
		System.out.println( "------Updated values-------");
		list.insertNth(7,7);
		list.insertNth(1,0);
		list.printList(list);
		System.out.println();
		System.out.println( "------Delete the values 9------");
		list.deleteNode(5);
//		System.out.println();
		list.printList(list);
		System.out.println();
		System.out.println( "------ center------");
		list.printMiddle();
		System.out.println( "------Sorting------");
		list.sortList();
		list.printList(list);
		System.out.println();
		System.out.println( "------Size------");
		System.out.println("The size of the linked list is: " + list.sizeofList());
		
		System.out.println( "------print the values------");
		list.printLinkedList();
		
	}
	
	

}
