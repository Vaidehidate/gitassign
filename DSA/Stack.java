package com.nagarro.stack;
//package Sample.stack;// Stack implementation in Java

public class Stack<I extends Number> extends StackMain{
   int arr[];
   int top;
   int capacity;
   
   

  // Creating a stack
  Stack(int size) 
  {
	
    arr = new int[size];
    capacity = size;
    top = -1;
    
    
  }

  // Add elements into stack
  public void push(int x) 
  {
    if (isFull()) 
    {
      System.out.println("OverFlow\nProgram Terminated\n");
      System.exit(1);
    }

    System.out.println("Insert " + x);
    arr[++top] = x;
  }

  // Remove element from stack
  public int pop() 
  {
    if (isEmpty()) 
    {
      System.out.println("STACK EMPTY");
      System.exit(1);
    }
    System.out.println("Removing " + peek());
    return arr[top--];
  }
  public int peek()
  {
      if (!isEmpty()) {
          return arr[top];
      }
      else {
          System.exit(-1);
      }

      return -1;
  }

  // Utility function to return the size of the stack
  public int size() 
  {
    return top + 1;
  }

  // Check if the stack is empty
  public Boolean isEmpty() 
  {
    return top == -1;
  }

  // Check if the stack is full
  public Boolean isFull() 
  {
    return top == capacity - 1;
  }

  public void printStack() 
  {
	 // ListIterator<Integer> lt = s.listIterator();
	  
    for (int i = 0; i <= top; i++) 
    {
      System.out.println(arr[i]);
    }
  }
  
  }
 

  










































//import java.util.*;
/*
class Stack
{
   private int arr[];
   private int top;
   private int capacity;

   // Constructor to initialize the stack
   Stack(int size)
   {
       arr = new int[size];
       capacity = size;
       top = -1;
   }

   // Utility function to add an element `x` to the stack
   public void push(int x)
   {
       if (isFull())
       {
           System.out.println("Overflow\nProgram Terminated\n");
           System.exit(-1);
       }

       System.out.println("Inserting " + x);
       arr[++top] = x;
   }
   public boolean isFull() {
       return top == capacity - 1;     // or return size() == capacity;
   }
}

	*/