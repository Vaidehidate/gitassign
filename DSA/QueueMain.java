package com.nagarro.queue;

import java.util.LinkedList;

public class QueueMain {

		public static void main (String[] args)
	    {
	        // create a queue of capacity 5
			
	        Queue q = new Queue(7);
	 
	        q.enqueue(1);
	        q.enqueue(2);
	        q.enqueue(3);
	        q.enqueue(5);
	        q.enqueue(6);
	        q.enqueue(9);
	        q.enqueue(10);

	        System.out.println("The front element is " + q.peek());
	        q.dequeue();
	      //  System.out.print(q.enqueue(7));
	        System.out.println("The peek element is " + q.peek());
	 
	        System.out.println("The queue size is " + q.size());
	        System.out.println("adding element randomly");
	        java.util.Queue<Integer> q1 = new LinkedList<>();
	        q1.add(10);
	        q1.add(7);
	        q1.add(2);
	        q1.add(8);
	        q1.add(6);
	        System.out.println("Sorted elements");
	        q.sortQueue(q1);
	    }
	}

	        

